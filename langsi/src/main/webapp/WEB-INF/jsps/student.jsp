<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script type="text/javascript">
function deleteStudents(){
	//提交form
	document.studentForm.action="deleteStudents.action";
	document.studentForm.submit();
}
function queryStudent(){
	//提交form
	document.studentForm.action="queryStudentByVo.action";
	document.studentForm.submit();
}
</script>

</head>
<body>
	hello 小魏!!!!
	${student.name}<br/>
	查询条件:
	<form action="queryStudentByVo.action" name="studentForm" method="post">
		<table width="25%" border=1>
			<tr>
				<td>商品名称:<input type="text" name="StudentCustom.name"/></td>
				<td><input type="button" value="查询" onclick="queryStudent()"/></td>
				<td><input type="button" value="批量删除" onclick="deleteStudents()"/></td>
			</tr>
		</table>
	
	<table>
		<thead>
			<th></th>
			<th>id</th>
			<th>name</th>
			<th>age</th>
			<th>createtime</th>
			<th>操作--><a href="addStudentView.action">添加</a></th>
		</thead>
		<tbody>
			<c:forEach items="${studentList}" var="student" varStatus="status">  
  				<tr >  
  				 <td><input type="checkbox" name="student_id" value="${student.id}"/></td>
   				 <td>${student.id}</td>  
    			 <td>${student.name}</td>  
     			 <td>${student.age}</td>
     			 <td><fmt:formatDate value="${student.createtime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
     			 <td><a href="deleteStudentById.action?id=${student.id}">删除</a>|<a href="updateStudentByIdView.action?id=${student.id}">修改</a></td>  
 			    </tr>  
			</c:forEach>  
		</tbody>
	</table>
	</form>
</body>
</html>