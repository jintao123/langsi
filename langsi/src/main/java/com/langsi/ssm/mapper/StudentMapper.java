
package com.langsi.ssm.mapper;

import java.util.List;

import com.langsi.ssm.po.Student;

/**
 * <p>ClassName:StudentMapper</p>
 * <p>Description:student的mapper操作接口</p>
 * <p> E-mail:jintao_iknow@163.com </p>
 * Date:     2016年4月20日 下午5:42:49 <br/>
 * @author   tao95
 * @version  1.0
 * @since    JDK 1.7
 *
 */
public interface StudentMapper {
	//查询学生信息，返回List
	public List<Student> selectStudentList() throws Exception;
}
