
package com.langsi.ssm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.langsi.ssm.po.Student;
import com.langsi.ssm.service.StudentService;

/**
 * <p>ClassName:StudentController</p>
 * <p>Description:</p>
 * <p> E-mail:jintao_iknow@163.com </p>
 * Date:     2016��4��20�� ����5:49:46 <br/>
 * @author   tao95
 * @version  1.0
 * @since    JDK 1.7
 *
 */
@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/queryStudent")
	public ModelAndView queryStudent(HttpServletRequest request) throws Exception {
		
		System.out.println("StudentController...............");
		ModelAndView modelAndView = new ModelAndView();
		List<Student> studentList = studentService.selectStudentList();
		modelAndView.addObject("studentList",studentList);
		modelAndView.setViewName("student");
		return modelAndView;
		
	}
}
