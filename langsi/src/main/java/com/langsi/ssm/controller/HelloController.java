
package com.langsi.ssm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;

/**
 * <p>ClassName:HelloController</p>
 * <p>Description:</p>
 * <p> E-mail:jintao_iknow@163.com </p>
 * Date:     2016年4月20日 下午5:26:26 <br/>
 * @author   tao95
 * @version  1.0
 * @since    JDK 1.7
 *
 */

@RequestMapping("/hello")
@Controller
public class HelloController{

	//查询学生信息，返回List
	@RequestMapping("/hello")
	public ModelAndView hello(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		System.out.println("hello spring-mvc");
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("info","hello langsi!!!!");
		modelAndView.setViewName("hello");
		return modelAndView;
	}

}
