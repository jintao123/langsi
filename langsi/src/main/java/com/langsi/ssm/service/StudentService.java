
package com.langsi.ssm.service;

import java.util.List;

import com.langsi.ssm.po.Student;

/**
 * <p>ClassName:StudentService</p>
 * <p>Description:</p>
 * <p> E-mail:jintao_iknow@163.com </p>
 * Date:     2016年4月20日 下午5:46:02 <br/>
 * @author   tao95
 * @version  1.0
 * @since    JDK 1.7
 *
 */
public interface StudentService {
	//查询学生信息，返回List
	public List<Student> selectStudentList() throws Exception;
}
