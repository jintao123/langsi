
package com.langsi.ssm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.langsi.ssm.mapper.StudentMapper;
import com.langsi.ssm.po.Student;
import com.langsi.ssm.service.StudentService;

/**
 * <p>ClassName:StudentServiceImpl</p>
 * <p>Description:</p>
 * <p> E-mail:jintao_iknow@163.com </p>
 * Date:     2016年4月20日 下午5:46:25 <br/>
 * @author   tao95
 * @version  1.0
 * @since    JDK 1.7
 *
 */
public class StudentServiceImpl implements StudentService{

	//查询学生信息，返回List
	@Autowired
	private StudentMapper studentMapper;
	public List<Student> selectStudentList() throws Exception {
		System.out.println("StudentServiceImpl.......");
		return studentMapper.selectStudentList();
		
	}

}
